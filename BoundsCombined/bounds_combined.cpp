#include<iostream>
/*The strength of these two new algorithm comes to light
*when we need to identify a collection of repeated
*instances of a given value in a collection.
*/
int* upperBound(int* first, int* last, int target)
{
	for (; first < last; ++first)//Walks the array,
	{
		if (*(first) > target)//check if the value exists in the array,
		{
			return first;//if him exists return the memory address,
		}
	}
	return 0;
}
int* lowerBound(int* first, int* last,int target)
{
	for (; first < last; ++first)//Walks the array,
	{
		if (*(first) == target)//check if the value exists in the array,
		{
			return first;//if him exists return the memory address,
		}
		if (*(first) > target)//if him not exists
		{
			return first-1;//the memory address returned is the memory address before.
		}
	}
	return 0;
}
void print(int* lower,int* upper)
{
	for (; lower < upper; ++lower)
	{
		std::cout<<*(lower)<<" ";
	}
	std::cout<<"\n";
}
int main()
{
    int data[]{ 0, 0, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6 };
    size_t sz = sizeof(data)/sizeof(data[0]);
    int target = 4;

    int * lower = lowerBound( data, data+sz, target );
    int * upper = upperBound( data, data+sz, target );

    print( lower, upper );
}
