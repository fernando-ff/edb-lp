#include <iostream>
#include <array>
int* binarySearch(int* first, int* last, int target)
{ 
	int count  = last - first;
	int step;
	while(count != 0)
	{
		step = count/2;
		int* mid = first + step;
		if (*(mid) == target)
		{
			return mid;
		}
		else if (*(mid) < target)
		{
			first = mid+ 1;
			count = count - (step+1);
		}
		else
		{
			count = step;
		}
	}
	return first+1;
}
/*Given a collection of sorted elements with possible repetition defined by the range [first,last),
*the upper bound should return a pointer to the first element in the range
*that is greater than a given target-value value, or last if no such element is found.
*/
int* upperBound(int* first, int* last, int target)
{
	first = binarySearch(first,last,target);
	/*for (; first < last; ++first)//Walks the array,
	{
		if (*(first) > target)//check if the value exists in the array,
		{
			return first;//if him exists return the memory address,
		}
	}*/
	return first;
}
int main(void)
{
	int* first;//Stores the memory address of the first element of the array.
	int* last;//Stores the array's back memory address.
	int* result;//Stores the value that will be return by function upperBound.
	int array[]{1,1,2,3,3,5,5,6,6,6,7,7,8};//Stores the values used in this example.
	int target{8};//Stores the value that will be found.
	
	first = array;
	last = first + 13;
	result = upperBound(first,last,target);
	std::cout<<"The valuer returned is:"<<*(result)<<std::endl;//Print the value returned.

}