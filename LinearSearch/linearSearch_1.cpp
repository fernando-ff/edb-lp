#include<iostream>

int lsearch1(int V[], size_t left, size_t right, int target)
{
    for (; left < right; ++left)
    {
        if (V[left] == target) return left;
    }
    return -1;
}
int main() {
    int V[]{ 9, 2, 1, 4, -2, 0, 5, 12 }; // The data array.
    size_t n = sizeof(V)/sizeof(V[0]); // Find out the array length.

    int target{ 12 };// The target we are looking for.

    // ===== Execute the linear search on the entire array.
    int idx = lsearch1( V, 0, n, target );
    // =====

    // Evaluate the result.
    if ( idx == -1 ) std::cout << "Could no find target!\n";
    else std::cout << "Find in the index at " << idx << "\n";

    return 0;
}
