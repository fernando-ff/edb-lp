#include<iostream>

int linearSearch(int V[], int n,int target)
{
	for (int i = 0; i < n; ++i)//walks the array
	{
	//if target exists in the array return the index value    
		if (V[i] == target)	return i;
	}
	return -1;//else return -1
}
int main()
{
	int V[]{9,2,1,4,-2,0,5,12};//the data array.
	int target{ 12};//the value to be found
	size_t n {sizeof(V)/sizeof(V[0])};// Find out the array length.
	
	// ===== Execute the linear search on the entire array.
	int indx = linearSearch(V,n,target);
   	
   	// Evaluate the result.
	if (indx == -1) std::cout<<"Could not find target!\n";
	else std::cout<<"Find in the index at "<<indx<<"\n";

	return 0;
}