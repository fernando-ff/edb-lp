#include <iostream>
int lsearch(int* first, int* last, int target)
{
	for (; first < last; ++first)//walks the array
	{
		//if target exists in the array return the value 
		if (*(first) == target) return*(first);
	}
	return -1;//else return -1
}
int main()
{
	int V[]{ 9, 2, 1, 4, -2, 0, 5, 12 };
	int target{0};
	size_t n{sizeof(V)/sizeof(V[0])};
	// ===== Execute the linear search on the entire array.
	int result = lsearch(V,V+n,target);

	// Evaluate the result.
	if (result == -1) std::cout<<"Could not find target!\n";
	else std::cout<<"Find the value  "<<result<<"\n";
	return 0;
}