#ifndef CARDS_LIB_H
#define CARDS_LIB_H

#include <iostream>
#include <vector>
#include <fstream>

struct Card;
//=====Predicados=====//
bool is_figure_find(Card c);
bool is_red_find(Card c);
bool is_heart_filter (Card c);
//=====Funçoes de busca=====//
int* find_if(std::vector<Card> &v, bool(*pred)(Card));
void filter(std::vector<Card> &v, bool(*pred)(Card));
//=====Função para abrir arquivo=====//
int openFile( std::vector<Card> &v);


#endif