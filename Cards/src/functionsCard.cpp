#include <iostream>//std::cout,std::cin
#include <vector>//std::vector<char> v;
#include <fstream>//std::ifstream,std::ofstream

#include "../include/cards_lib.h"

struct Card {
    enum class suit : int
    {
        clubs    = 0,
        diamonds = 1,
        hearts   = 2,
        spades   = 3,
    };

    suit s;
    unsigned short r; // rank
};

//>>>>> Predicados <<<<<//

bool is_figure_find(Card c){
	//===== Descrição: verifica se a carta é uma figura =====//
	if (c.r > 10) return true;//Se a carta for uma figura,retorna verdadeiro.
	else return false;//Caso não seja retorna false.
}

bool is_red_find(Card c){
    //===== Descrição: verifica se a carta é vermelha =====//
	if (c.s == (Card::suit)0 or c.s == (Card::suit)2) return true;//Se a carta for vermelha,retorna verdadeiro.
	else return false;//Se não retorna falso.
}

bool is_heart_filter (Card c){
    //===== Descrição: verifica se a carte é de coração =====//
    if (c.s == (Card::suit)2) return true;//Se a carta for de coração,retorna verdadeiro.
    else return false;//Caso não seja retorna false.
}

//>>>>> Funções para encontrar uma carta de acordo com o predicado <<<<<//

int* find_if(std::vector<Card> &v, bool(*pred)(Card)){
    //===== Descrição:Seleciona um elemento do vector de acordo com o predicado =====//
	for (size_t i = 0; i < v.size(); ++i)//Percorre todo o vector.
		if (pred(v[i]) == true) return &v[i];//Se o predicado for verdadeiro, retorna 1.
	return 0;//Se o predicado for falso, retorna 0.
}

void filter(std::vector<Card> &v, bool(*pred)(Card)){
	//===== Descrição:Filtra as cartas de acordo com o predico =====//
    std::vector<Card> v_aux;
    for (size_t i{0}; i < v.size(); ++i)//Percorre todo o vector.
	{
		if (pred(v[i]) == true)//Verifica se o valor retornado pelo predicado é verdadeiro.
			v_aux.push_back(v[i]);//Adiciona o valor de v em v_aux.
    }
    v = v_aux;//Transfere os dados do vector v_aux para v.
}

//===== Função para abrir e fazer a leitura do arquivo =====//

int openFile( std::vector<Card> &v){
    std::ifstream ifs_;//Declaração do fluxo.
    ifs_.open("input.txt");//Abre o arquivo para leitura.
    Card c;//Armazena os valores que irão ser inseridos no vector.
    int val1;//Armaneza os valores lidos do arquivo pelo fluxo.
    unsigned short val2;//Armaneza os valores lidos do arquivo pelo fluxo.
    while(true){
        ifs_ >> val1 >> val2;//Transfere os valores lidos para as variaveis.
        if(ifs_.eof()){//Testa se o arquivo chegou ao fim ou se houve erro na leitura.
            std::cerr << "Data error in file OR end file!" <<std::endl ;
            return EXIT_FAILURE;//Retorna 1.
        }
        c.s =(Card::suit) val1;//Atribui os valores lidos ao struct.
        c.r = val2;//Atribui os valores lidos ao struct.
        v.push_back(c);//Armazena o struct na ultima posição do vector.
    }
    ifs_.close();//Fecha o arquivo.
    return 0;
}
